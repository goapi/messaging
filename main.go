package main

import "gitlab.com/trungtvq/messaging/cmd"

var revision string

func main() {
	cmd.SetRevision(revision)
	cmd.Execute()
}
