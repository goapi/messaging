package monitor

import (
	"context"

	"github.com/spf13/viper"
	"github.com/trungtvq/go-utils/service/monitor/adapter/tracing"

	"github.com/opentracing/opentracing-go"
	tags "github.com/opentracing/opentracing-go/ext"
)

var (
	//Tracer instance
	Tracer opentracing.Tracer
)

// CloneSpan copy content of tracer and context to new span, context
func CloneSpan(ctx context.Context, operationName string) (opentracing.Span, context.Context) {
	if span := opentracing.SpanFromContext(ctx); span != nil {
		span := Tracer.StartSpan(operationName, opentracing.ChildOf(span.Context()))
		tags.SpanKindRPCServer.Set(span)
		tags.PeerService.Set(span, "zpigw-service")
		return span, opentracing.ContextWithSpan(context.Background(), span)
	}
	return Tracer.StartSpan(operationName), context.Background()
}

// FollowSpan copy content of tracer and context to new span, context
func FollowSpan(ctx context.Context, operationName string) (opentracing.Span, context.Context) {
	if span := opentracing.SpanFromContext(ctx); span != nil {
		span := Tracer.StartSpan(operationName, opentracing.FollowsFrom(span.Context()))
		tags.SpanKindRPCServer.Set(span)
		tags.PeerService.Set(span, "zpigw-service")
		return span, opentracing.ContextWithSpan(context.Background(), span)
	}
	return Tracer.StartSpan(operationName), context.Background()
}

// InitTracer init tracer
func InitTracer() {
	Tracer = tracing.Init(viper.GetString("service.name"), Logger)
}
