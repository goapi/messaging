package monitor

import (
	"fmt"

	"github.com/spf13/viper"
	"github.com/trungtvq/go-utils/service/monitor/adapter/log"
)

var (
	//Logger log factory
	Logger     log.Factory
	HTTPLogger log.Factory
)

// InitLogger init log
func InitLogger() {
	logPath := viper.GetString("log.path")
	serviceName := viper.GetString("service.name")
	if logPath == "" || serviceName == "" {
		fmt.Println("Log service not working! Because it is not configured!")
		return
	}
	Logger = log.NewStandardFactory(logPath, serviceName, "debug")
	HTTPLogger = log.NewStandardFactory(logPath, serviceName+"-http", "debug")

	path := logPath + "/" + serviceName
	fmt.Println("Write log to", path, "&&", fmt.Sprintf("%s-http", path))
}

//InitLoggerForTest .
func InitLoggerForTest() {
	Logger = log.NewStandardFactory("./test-log/", viper.GetString("service.name"), "debug")
	fmt.Println(fmt.Sprintf("Write log to ./test-log/%s-debug level", viper.GetString("service.name")))
}
